import android.content.*;
import android.content.res.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.os.*;
import android.util.*;
import android.view.*;
import android.widget.*;

import static android.view.Gravity.*;

public class FloatingButton extends LinearLayout {
	
	private static int BUTTON_SIZE = 64, DEFAULT_ICON_COLOR = 0xFFFFFFFF, clr, iclr;
	private static boolean REVERSE = false, EXCEPTION = false, OLD_REVERSE = false;
	private LinearLayout btns = null;
	private ImageView main = null;
	private ViewGroup sv = null;
	private View bf = null, va[] = null;
	private Orientation oldOri = null;
	
	public FloatingButton(Context c){
		this(c,-1);
	}
	
	public FloatingButton(Context c,Orientation ori){
		this(c,ori,-1);
	}
	
	public FloatingButton(Context c,Orientation ori,int color){
		this(c,ori,color,DEFAULT_ICON_COLOR);
	}
	
	public FloatingButton(Context c,int color){
		this(c,color,DEFAULT_ICON_COLOR);
	}
	
	public FloatingButton(Context c,int bgColor,int iconColor){
		this(c,Orientation.BRV,bgColor,iconColor);
	}
	
	public FloatingButton(Context c,Orientation ori,int bgColor,int iconColor){
		super(c);
		setLayoutParams(new LayoutParams(-1,-1));
		if(bgColor != -1){
			clr = bgColor;
		} else {
			clr = getAccentColorOrDefault();
		}
		iclr = iconColor;
		setOrientation(ori);
		addButton(SubButton.newButton(c.getResources(),android.R.drawable.ic_input_add,null));
	}
	
	@Override
	public int getOrientation(){
		if(EXCEPTION){
			setOrientation(-100);
			return -1;
		}
		EXCEPTION = true;
		return super.getOrientation();
	}
	
	public Orientation getFBOrientation(){
		return oldOri;
	}
	
	public void setOrientation(Orientation ori){
		if(ori != oldOri){
			EXCEPTION = false;
			REVERSE = false;
			switch(ori){
				case BLV:
					setGravity(BOTTOM | LEFT);
					setOrientation(VERTICAL);
					REVERSE = true;
					break;
				case BLH:
					setGravity(BOTTOM | LEFT);
					setOrientation(HORIZONTAL);
					break;
				case BRV:
					setGravity(BOTTOM | RIGHT);
					setOrientation(VERTICAL);
					REVERSE = true;
					break;
				case BRH:
					setGravity(BOTTOM | RIGHT);
					setOrientation(HORIZONTAL);
					REVERSE = true;
					break;
				case TLV:
					setGravity(TOP | LEFT);
					setOrientation(VERTICAL);
					break;
				case TLH:
					setGravity(TOP | LEFT);
					setOrientation(HORIZONTAL);
					break;
				case TRV:
					setGravity(TOP | RIGHT);
					setOrientation(VERTICAL);
					break;
				case TRH:
					setGravity(TOP | RIGHT);
					setOrientation(HORIZONTAL);
					REVERSE = true;
					break;
			}
			if(btns != null){
				btns.setOrientation(getOrientation());
				va = new View[btns.getChildCount()];
				for(int i = 0;i < va.length;i++){
					va[i] = btns.getChildAt(i);
				}
				setScrollView();
				for(int i = 0;i < va.length;i++){
					add(btns,va[i],OLD_REVERSE);
				}
				if(REVERSE){
					addView(bf);
					addView(sv);
					addView(main);
				} else {
					addView(main);
					addView(sv);
					addView(bf);
				}
				va = null;
			}
			EXCEPTION = true;
			OLD_REVERSE = REVERSE;
			oldOri = ori;
		}
	}
	
	public void addButton(Resources r, int res, OnButtonClickListener ocl){
		addButton(SubButton.newButton(r,res,ocl));
	}
	
	public void addButton(int res, OnButtonClickListener ocl){
		addButton(SubButton.newButton(res,ocl));
	}
	
	public void addButton(Drawable icon, OnButtonClickListener ocl){
		addButton(SubButton.newButton(icon,ocl));
	}
	
	public void addButton(SubButton sb){
		ImageView b = new ImageView(getContext());
		b.setImageDrawable(sb.i);
		int p = dp(16);
		b.setPadding(p,p,p,p);
		b.setBackgroundDrawable(getButtonBackground());
		b.setScaleType(ImageView.ScaleType.FIT_CENTER);
		b.setOnClickListener(sb.o != null ? sb.o : new OnButtonClickListener(this));
		if(getChildCount() != 0){
			b.setScaleX(0);
			b.setScaleY(0);
		}
		p = dp(BUTTON_SIZE);
		if(btns == null){
			addView(main = b);
			b.setTag(getChildCount());
			b.setLayoutParams(new LayoutParams(p,p,0));
			btns = new LinearLayout(getContext());
			EXCEPTION = false;
			btns.setOrientation(getOrientation());
			btns.setLayoutParams(new ScrollView.LayoutParams(-2,-2));
			setScrollView();
			add(this,sv);
			// BUG FIX ITEM, DON'T DELETE IT
			bf = new View(getContext());
			bf.setLayoutParams(new LayoutParams(p,p,0));
			add(this,bf);
		} else {
			b.setLayoutParams(new LayoutParams(p,p));
			b.setTag(btns.getChildCount());
			add(btns,b);
		}
	}
	
	private void setScrollView(){
		if(sv != null){
			btns.removeAllViewsInLayout();
			sv.removeAllViewsInLayout();
			removeAllViewsInLayout();
		}
		sv = (btns.getOrientation() == VERTICAL) 
			? new ScrollView(getContext()) 
			: new HorizontalScrollView(getContext());
		sv.setLayoutParams(new LayoutParams(-2,-2,0));
		sv.setVisibility(View.GONE);
		sv.addView(btns);
		sv.setHorizontalScrollBarEnabled(false);
		sv.setVerticalScrollBarEnabled(false);
	}

	@Override
	public void setOrientation(int orientation){
		if(EXCEPTION){
			throw new RuntimeException("Incompatible method for "+getClass().getSimpleName());
		} else {
			super.setOrientation(orientation);
		}
	}
	
	private int getAccentColorOrDefault(){
		if(Build.VERSION.SDK_INT > 20){
			TypedValue typedValue = new TypedValue();
			getContext().getTheme().resolveAttribute(android.R.attr.colorAccent, typedValue, true);
			return typedValue.data;
		}
		return 0xFF6699FF;
	}

	private Drawable getButtonBackground(){
		GradientDrawable gd = new GradientDrawable();
		gd.setShape(GradientDrawable.OVAL);
		gd.setStroke(dp(12),0,0,0);
		gd.setColor(clr);
		GradientDrawable ld = new GradientDrawable();
		ld.setShape(GradientDrawable.OVAL);
		ld.setStroke(dp(8),0,0,0);
		ld.setColor(clr - 0xBB000000);
		LayerDrawable l = new LayerDrawable(new Drawable[]{ld,gd});
		return l;
	}
	
	private void add(ViewGroup g, View v){
		add(g,v,REVERSE);
	}
	
	private void add(ViewGroup g, View v, boolean force){
		if(force){
			g.addView(v,0);
		} else {
			g.addView(v);
		}
	}
	
	private static int dp(int px){
		return (int)(Resources.getSystem().getDisplayMetrics().density * px);
	}
	
	public static class OnButtonClickListener implements OnClickListener {
		
		private static FloatingButton par = null;
		int baseDelay = 25;
		
		public OnButtonClickListener(FloatingButton parent){
			if(parent == null){
				throw new RuntimeException(OnButtonClickListener.class.getSimpleName()+" parent must be declared");
			} else {
				par = parent;
			}
		}
		
		@Override
		public void onClick(View v){
			if(par.getChildCount() != 1){
				if(par.btns.getChildAt(0).getScaleX() == 0){
					par.sv.setVisibility(VISIBLE);
					par.main.animate().rotation(135);
				} else {
					par.main.animate().rotation(0);
				}
				for(int i = 0;i < par.btns.getChildCount();i++){
					final int g = i,d1 = Math.abs((par.btns.getChildCount()-1)-(i+1))*baseDelay,d2 = (i+1)*baseDelay;
					if(par.btns.getChildAt(0).getScaleX() == 0){
						par.btns.getChildAt(i).animate().scaleX(1).scaleY(1).setStartDelay(par.REVERSE ? d1 : d2).withStartAction(new Runnable(){
								public void run(){
									par.btns.getChildAt(g).setVisibility(View.VISIBLE);
									if(g == (par.REVERSE ? (par.btns.getChildCount()-1) : 0)){
										if(REVERSE){
											par.sv.setScrollX(par.btns.getChildCount()*dp(BUTTON_SIZE));
											par.sv.setScrollY(par.btns.getChildCount()*dp(BUTTON_SIZE));
										} else {
											par.sv.setScrollX(0);
											par.sv.setScrollY(0);
										}
									}
								}
							});
					} else {
						par.btns.getChildAt(i).animate().scaleX(0).scaleY(0).setStartDelay(par.REVERSE ? d2 : d1).withEndAction(new Runnable(){
								public void run(){
									par.btns.getChildAt(g).setVisibility(View.INVISIBLE);
									if(g == (par.REVERSE ? (par.btns.getChildCount()-1) : 0)){
										par.sv.setVisibility(GONE);
									}
								}
							});
					}
				}
			}
		}
	}
	
	public static class SubButton {
		
		private SubButton(){}
		
		private static SubButton s = null;
		private static Drawable i = null;
		private static OnButtonClickListener o = null;
		
		public static SubButton newButton(Drawable img, OnButtonClickListener ocl){
			s.i = img;
			s.i.setColorFilter(iclr,PorterDuff.Mode.SRC_ATOP);
			s.o = ocl;
			return s;
		}
		
		public static SubButton newButton(int res, OnButtonClickListener ocl){
			return newButton(Resources.getSystem().getDrawable(res),ocl);
		}
		
		public static SubButton newButton(Resources r, int res, OnButtonClickListener ocl){
			return newButton(r.getDrawable(res),ocl);
		}
		
	}
	
	public static enum Orientation { BRH, BRV, BLH, BLV, TRH, TRV, TLH, TLV }
}
