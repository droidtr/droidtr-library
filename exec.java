import android.os.*;
import java.io.*;

class exec
{

	public static void execNoWait(final String cmd){
		execNoWait(cmd,"su");
	}
	public static String execForStringOutput(String cmd){
		return execForStringOutput(cmd,"su");
	}
	public static void execNoWait(final String command,final String shell){
		new AsyncTask<String,String,Void>(){
			@Override
			public Void doInBackground(String... params){
				execForStringOutput(command,shell);
				return null;
			}
		}.execute();
	}
	public static String execForStringOutput(String command,String shell) {
		try{
			String line;
			StringBuilder s = new StringBuilder();
			java.lang.Process p = Runtime.getRuntime().exec(shell);
			DataOutputStream dos = new DataOutputStream(p.getOutputStream());
			String[] clist=command.split(";");
			for(int i=0;i<clist.length;i++){
				dos.writeBytes(clist[i]+"\n");
				dos.flush();
			}
			dos.close();
			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = input.readLine()) != null){
				if(line.trim().length()>0){
					s.append((line).trim()+"\n");
				}
			}
			input.close();
			return s.toString();
		}catch(Exception e){
			return "";
		}
	}
}