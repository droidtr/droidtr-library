package org.droidtr.kbd;
import android.content.*;
import android.view.*;
import android.view.inputmethod.*;
import android.widget.*;
import android.inputmethodservice.*;
import android.graphics.drawable.*;
import android.graphics.*;
import android.os.*;
import java.util.*;
public class CustomKeyboardView extends RelativeLayout{
	Context ctx=null;
	//paylaşılmayan nesneler
	private LinearLayout.LayoutParams butparam =null;
	private LinearLayout[] main =null;
	private key[][][] b = null;
	private LinearLayout[][] ll=null;
	private LinearLayout popupll =null;
	private int i=0;
	private int[] rmax=new int[50];
	private int[][] bmax=new int[50][50];
	private key[] popupbuts =new key[12];
	private int mmax=0;
	private int getActiveKeyboard=0;
	private int status=0;
	private long time=0;
	private boolean tmp=false;
	private GradientDrawable keyboardBackgroundDrawable=null;
	//paylaşılan nesneler
	public Drawable[] Backgrounds=new Drawable[10];
	public Drawable bg=null;
	public Drawable bgon=null;
	public Drawable bgdown=null;
	public int keyboardBackground=Color.parseColor("#282d31");
	public int defaultButtonColor=Color.BLACK;
	public int secondaryButtonColor=Color.BLACK;
	public int defaultButtonRadius=18;
	public Runnable prePop=null;
	public Runnable postPop=null;
	public Runnable preInt=null;
	public Runnable postInt=null;
	public OnTouchListener toucharea=null;
	public boolean ctrl=false;
	public boolean alt=false;
	public int popupDuration=250;
	public int popupHeight=100;
	public int vibration=35;
	//Gerekli nesneler
	Random r=new Random();
	Handler h = new Handler();
	CustomKeyboardView(Context c){
		super(c);
		ctx=c;
		butparam=new LinearLayout.LayoutParams(-1,-1);
		butparam.weight=1.0f;
		main=new LinearLayout[50];
		createKeyboard(1);
		bgdown=gd(Color.parseColor("#99874b4c"));
		bgon=gd(Color.parseColor("#99ff4b4c"));
		bg=gd(Color.parseColor("#99474b4c"));
		defaultButtonColor=Color.parseColor("#dde1e2");
		secondaryButtonColor=Color.parseColor("#aadde1e2");
		keyboardBackgroundDrawable=(GradientDrawable)gd(keyboardBackground);
		keyboardBackgroundDrawable.setStroke(0,0);
		this.setBackgroundDrawable(keyboardBackgroundDrawable.getConstantState().newDrawable());
		this.setPadding(10,10,10,10);
		ll = new LinearLayout[50][400];
		b = new key[50][50][400];
	}
	public void addrow(String[][] rows,int index){
		for(int i=0;i<rows.length;i++){
			addrow(rows[i],index);
		}
	}
	public void addrow(String[][][] rows){
		for(int j=0;j<rows.length;j++){
			for(int i=0;i<rows.length;i++){
				addrow(rows[i],j+mmax);
			}
		}
	}
	public int getActiveKeyboard(){
		return getActiveKeyboard;
	}
	public void createKeyboard(int loop){
		for(i=0;i<loop;i++){
			main[mmax] = new LinearLayout(ctx);
			main[mmax].setLayoutParams(new LayoutParams(-1,-1));
			main[mmax].setOrientation(LinearLayout.VERTICAL);
			main[mmax].setVisibility(View.INVISIBLE);
			this.addView(main[mmax]);
			rmax[mmax]=0;
			mmax=mmax+1;
		}
	}
	public void setHeight(int index,int height){
		popupHeight=height/6;
		main[index].setLayoutParams(new LayoutParams(-1,height));
	}
	public void setHeight(int height){
		popupHeight=height/6;
		for(i=0;i<mmax;i++){
			main[i].setLayoutParams(new LayoutParams(-1,height));
		}
	}
	public void createKeyboard(){
		createKeyboard(1);
	}
	public int getKeyboardHeight(int index){
		return main[index].getLayoutParams().height;
	}
	public void addrow(final String[] keys,int index){
		ll[index][rmax[index]] = new LinearLayout(ctx);
		bmax[index][rmax[index]]=0;
		for(i=0;i<keys.length;i++){
			b[index][rmax[index]][i] = new key(ctx);
			b[index][rmax[index]][i].setGravity(Gravity.CENTER);
			String keylabel=decode(keys[i])[0];
			if(keylabel.contains("::")){
				b[index][rmax[index]][i].setTag(keylabel.split("::")[0]);
				b[index][rmax[index]][i].setText(keylabel.split("::")[1]);
			}else{
				b[index][rmax[index]][i].setText(keylabel);
			}
			b[index][rmax[index]][i].setLayoutParams(butparam);
			b[index][rmax[index]][i].setOnTouchListener(ocl);
			b[index][rmax[index]][i].popupList=decode(keys[i]);
			if(b[index][rmax[index]][i].popupList.length>1){
			b[index][rmax[index]][i].setSecondaryText(decode(keys[i])[1]);
			}
			bmax[index][rmax[index]]++;
			ll[index][rmax[index]].addView(b[index][rmax[index]][i]);
		}
		ll[index][rmax[index]].setLayoutParams(butparam);
		main[index].addView(ll[index][rmax[index]]);
		rmax[index]=rmax[index]+1;
	}
	public key getButton(int keyboard,int row,int index){
		if(keyboard<0){
			keyboard=mmax+keyboard;
		}
		if(row<0){
			row=rmax[keyboard]+row;
		}
		if(index<0){
			index=bmax[keyboard][row]+index;
		}
		if(b[keyboard][row][index]!= null){
			return b[keyboard][row][index];
		}else{
			return new key(ctx);
		}
	}
	public int randomColor(int min,int max){
		return (Color.rgb(max-r.nextInt(min),max-r.nextInt(min),max-r.nextInt(min)));
	}
	private void interract(final View p1){
		if(preInt!=null){
			h.post(preInt);
		}
		h.post(new Runnable(){

				@Override
				public void run()
				{
					if(p1.getTag()!=null){
						getCurrentInputConnection().sendKeyEvent(setctrlalt(Integer.parseInt(p1.getTag().toString())));
					}else{
						String keylabel=((key)p1).getText().toString();
						int keycode=keylabel.toLowerCase().toCharArray()[0];
						int keycodeorj=keylabel.toCharArray()[0];
						if((ctrl||alt) &&keycode>=97 && keycode <=122 &&keylabel.length()<2){
							getCurrentInputConnection().sendKeyEvent(setctrlalt(keycode-68,ctrl,alt,keycode!=keycodeorj));
						}else{
							getCurrentInputConnection().commitText(keylabel,1);
						}
				}
				}
			});
		
		if(postInt!=null){
			h.post(postInt);
		}
	}
	public LinearLayout getRow(int keyboard,int index){
		if(ll[index]!= null){
			return ll[keyboard][index];
		}else{
			return new LinearLayout(ctx);
		}
	}
	public LinearLayout getKeyboard(int index){
		if(main[index]!= null){
			return main[index];
		}else{
			return new LinearLayout(ctx);
		}
	}
	public void setActiveKeyboard(int index){
		getActiveKeyboard=index;
		if(main[index].getVisibility()==View.INVISIBLE){
			for(i=0;i<mmax;i++){
				main[i].setVisibility(View.INVISIBLE);
			}
			main[index].setVisibility(View.VISIBLE);
		}
	}
	public KeyEvent setctrlalt(int keycode){
		return setctrlalt(keycode,false,false,false);
	}
	public KeyEvent setctrlalt(int keycode,boolean ctrl,boolean alt,boolean caps){
		if(!caps){
			if(ctrl){
				if(alt){
					return new KeyEvent(SystemClock.uptimeMillis(),SystemClock.uptimeMillis(),KeyEvent.ACTION_DOWN,keycode,0,KeyEvent.META_CTRL_ON |KeyEvent.META_ALT_ON,0,0,KeyEvent.FLAG_SOFT_KEYBOARD | KeyEvent.FLAG_KEEP_TOUCH_MODE,InputDevice.SOURCE_KEYBOARD);
				}else{
					return new KeyEvent(SystemClock.uptimeMillis(),SystemClock.uptimeMillis(),KeyEvent.ACTION_DOWN,keycode,0,KeyEvent.META_CTRL_ON,0,0,KeyEvent.FLAG_SOFT_KEYBOARD | KeyEvent.FLAG_KEEP_TOUCH_MODE,InputDevice.SOURCE_KEYBOARD);
				}
			}else{
				if(alt){
					return new KeyEvent(SystemClock.uptimeMillis(),SystemClock.uptimeMillis(),KeyEvent.ACTION_DOWN,keycode,0,KeyEvent.META_ALT_ON,0,0,KeyEvent.FLAG_SOFT_KEYBOARD | KeyEvent.FLAG_KEEP_TOUCH_MODE,InputDevice.SOURCE_KEYBOARD);
				}else{ 
					return new KeyEvent(SystemClock.uptimeMillis(),SystemClock.uptimeMillis(),KeyEvent.ACTION_DOWN,keycode,0,0,0,0,KeyEvent.FLAG_SOFT_KEYBOARD | KeyEvent.FLAG_KEEP_TOUCH_MODE,InputDevice.SOURCE_KEYBOARD);
				}
			}
		}else{
			if(ctrl){ 
				if(alt){
					return new KeyEvent(SystemClock.uptimeMillis(),SystemClock.uptimeMillis(),KeyEvent.ACTION_DOWN,keycode,0,KeyEvent.META_CTRL_ON | KeyEvent.META_ALT_ON | KeyEvent.META_SHIFT_ON,0,0,KeyEvent.FLAG_SOFT_KEYBOARD | KeyEvent.FLAG_KEEP_TOUCH_MODE,InputDevice.SOURCE_KEYBOARD);
				}else{
					return new KeyEvent(SystemClock.uptimeMillis(),SystemClock.uptimeMillis(),KeyEvent.ACTION_DOWN,keycode,0,KeyEvent.META_CTRL_ON | KeyEvent.META_SHIFT_ON ,0,0,KeyEvent.FLAG_SOFT_KEYBOARD | KeyEvent.FLAG_KEEP_TOUCH_MODE,InputDevice.SOURCE_KEYBOARD);
				}
			}else {
				if(alt){
					return new KeyEvent(SystemClock.uptimeMillis(),SystemClock.uptimeMillis(),KeyEvent.ACTION_DOWN,keycode,0,KeyEvent.META_ALT_ON | KeyEvent.META_SHIFT_ON,0,0,KeyEvent.FLAG_SOFT_KEYBOARD | KeyEvent.FLAG_KEEP_TOUCH_MODE,InputDevice.SOURCE_KEYBOARD);
				} else{
					return new KeyEvent(SystemClock.uptimeMillis(),SystemClock.uptimeMillis(),KeyEvent.ACTION_DOWN,keycode,0,KeyEvent.META_SHIFT_ON,0,0,KeyEvent.FLAG_SOFT_KEYBOARD | KeyEvent.FLAG_KEEP_TOUCH_MODE,InputDevice.SOURCE_KEYBOARD);
				}
			}
		} 
	} 
	public OnTouchListener ocl = new OnTouchListener(){
		@Override
		public boolean onTouch(final View p1, final MotionEvent p2){
			if(p2.getAction() == MotionEvent.ACTION_DOWN){
				vibrate();
				if(bgdown!=null){
					((key)p1).setBackgroundDrawable(bgdown,false);
				}
				time=System.currentTimeMillis();
				tmp=false;
				h.post(new Runnable(){
						@Override
						public void run(){
							Long newtime=System.currentTimeMillis();
							if(!tmp){
								if(newtime-time<popupDuration){
									h.postDelayed(this,50);
								}else{
									vibrate();
									tmp=true;
									LongClick(p1,p2);
								}}
						}
					});
			}
			if(p2.getAction() == MotionEvent.ACTION_UP){
				if(!tmp){
					interract(p1);
				}
				tmp=true;
				if(bg!=null){
					((key)p1).setBackgroundDrawable(((key)p1).backgroundDrawable,false);
					if(status!=2 && getActiveKeyboard==1){
						setActiveKeyboard(0);
					} 
				}
			}
			return true;
		}
	};
	OnTouchListener setActiveKeyboardEvent(int index){
		return setActiveKeyboardEvent(index,false);
	}
	OnTouchListener setActiveKeyboardEvent(final int index,final boolean isShift){
		return new OnTouchListener(){
			@Override
			public boolean onTouch(final View p1, final MotionEvent p2){
				if(p2.getAction() == MotionEvent.ACTION_DOWN){
					vibrate();
					if(isShift){
						if(status !=1){
							setActiveKeyboard(index);
						}
						if(status==0){
							status=1;
						}else if(status==1){
							status=2;
						}else{
							status=0;
						}
					}else{
						setActiveKeyboard(index);
					}
				}
				return true;
			}
		};
	}
	//tuş classı
	class key extends RelativeLayout {
		public TextView tv=null;
		public TextView pv=null;
		public ImageView icon=null;
		public Drawable backgroundDrawable=null;
		public Runnable longClick=null;
		public String[] popupList={};
		public key(Context c){
			super(c);
			tv =new TextView(c);
			pv=new TextView(c);
			pv.setTextSize(tv.getTextSize()/4);
			icon =new ImageView(c);
			setBackgroundDrawable(bg.getConstantState().newDrawable());
			tv.setTextColor(defaultButtonColor);
			pv.setTextColor(secondaryButtonColor);
			addView(tv);
			pv.setX(30);
			pv.setY(-20);
			addView(pv);
			addView(icon);
		}
		public void setText(String text){
			tv.setText(text);
			icon.setBackground(null);
		}
		public void setSecondaryText(String text){
			pv.setText(text);
			icon.setBackground(null);
		}
		public void setIcon(Drawable drawable){
			tv.setText("");
			icon.setBackgroundDrawable(drawable);
		}
		public ImageView getIcon(){
			return icon;
		}
		public void setIcon(int resource){
			tv.setText("");
			icon.setBackgroundResource(resource);
		}
		public CharSequence getText(){
			return tv.getText();
		}

		public void setBackgroundDrawable(Drawable background,boolean backup){
			super.setBackgroundDrawable(background.getConstantState().newDrawable());
			if(backup){
				backgroundDrawable=background;
			}
		}
		@Override
		public void setBackgroundDrawable(Drawable background){
			backgroundDrawable=background;
			super.setBackgroundDrawable(background.getConstantState().newDrawable());
		}
	}
	public void setKeyBackground(int keyboard,int row,int index,int num){
		if(Backgrounds[i]==null){
			Backgrounds[i]=bg.getConstantState().newDrawable();
		}
		getButton(keyboard,row,index).setBackgroundDrawable(Backgrounds[num].getConstantState().newDrawable());
	}
	public void setKeyBackground(int[][] list){
		for (int i=0;i<list.length;i++){
			setKeyBackground(list[i][0],list[i][1],list[i][2],list[i][3]);
		}
	}
	//tuşlar için drawable
	public Drawable gd(int[] col){
		GradientDrawable gd = new GradientDrawable();
		gd.setColors(col);
		gd.setCornerRadius(defaultButtonRadius);
		gd.setStroke(10,Color.TRANSPARENT);
		return gd.getConstantState().newDrawable();
	}
	//tuşlar için drawable
	Drawable gd(int col){
		int[] cols={col,Color.argb(Color.alpha(col),8*Color.red(col)/9,8*Color.green(col)/9,8*Color.blue(col)/9),col};
		return gd(cols);
	}
	//ic çekiliyor
	public InputConnection getCurrentInputConnection(){
		return ((InputMethodService) getContext()).getCurrentInputConnection();
	}
	public void setRepeat(int[][] list){
		for (int i=0;i<list.length;i++){
			setRepeat(list[i][0],list[i][1],list[i][2]);
		}
	}
	//tekrarlı hale getirmek için
	public void setRepeat(int keyboard,int row,int index){
		getButton(keyboard,row,index).setOnTouchListener(new OnTouchListener(){
				@Override
				public boolean onTouch(final View p1, final MotionEvent p2){
					if(p2.getAction() == MotionEvent.ACTION_UP){
						if(bg!=null){
							((key)p1).setBackgroundDrawable(((key)p1).backgroundDrawable,false);
						}
					}
					if(p2.getAction() == MotionEvent.ACTION_DOWN){
						if(bgdown!=null){
							((key)p1).setBackgroundDrawable(bgdown,false);
						}
						i=0;
						vibrate();
						interract(p1);
						h.postDelayed(new Runnable(){
								@Override
								public void run(){
									if(p2.getAction() != MotionEvent.ACTION_UP){
										vibrate();
										interract(p1);
										if(i<80){
											i=i+3;
										}
										h.postDelayed(this,120-i);
									}
								}
							},120);
					}
					return true;
				}
			});
	}
	//ctrl açma kapatma eventi
	public OnTouchListener ctrlonoff(){
		return new OnTouchListener(){
			@Override
			public boolean onTouch(View p1, MotionEvent p2){
				if(p2.getAction() == MotionEvent.ACTION_DOWN){
					ctrl=!ctrl;
					vibrate();
					if(ctrl){
						((key)p1).setBackgroundDrawable(bgon,false);
					}
					else{
						((key)p1).setBackgroundDrawable(((key)p1).backgroundDrawable,false);
					}
				}
				return false;
			}
		};
	}
	//alt açma kapatma eventi
	public OnTouchListener altonoff(){
		return new OnTouchListener(){
			@Override
			public boolean onTouch(View p1, MotionEvent p2){
				if(p2.getAction() == MotionEvent.ACTION_DOWN){
					ctrl=!ctrl;
					vibrate();
					if(ctrl){
						((key)p1).setBackgroundDrawable(bgon,false);
					}
					else{
						((key)p1).setBackgroundDrawable(((key)p1).backgroundDrawable,false);
					}
				}
				return false;
			}
		};
	}
	//popup arayüzü
	View popuplayout(){
		popupll= new LinearLayout(ctx);
		LinearLayout altview1 =new LinearLayout(ctx);
		LinearLayout altview2 =new LinearLayout(ctx);
		LinearLayout altview3 =new LinearLayout(ctx);
		key exit = new key(ctx);
		exit.setLayoutParams(new LayoutParams(popupHeight,popupHeight));
		exit.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View p1){
					popupll.setVisibility(View.GONE);
				}
			});
		altview1.addView(exit);
		popupll.setOrientation(LinearLayout.VERTICAL);
		popupll.setVisibility(View.GONE);
		popupll.setGravity(Gravity.CENTER);
		popupll.setBackgroundDrawable(keyboardBackgroundDrawable.getConstantState().newDrawable());
		popupll.setPadding(10,10,10,10);
		altview1.setLayoutParams(new LayoutParams(-2,-1));
		altview2.setLayoutParams(new LayoutParams(-2,-1));
		altview3.setLayoutParams(new LayoutParams(-2,-1));
		for(i=0;i<popupbuts.length-1;i++){
			popupbuts[i]=new key(ctx);
			popupbuts[i].setGravity(Gravity.CENTER);
			popupbuts[i].setText("");
			popupbuts[i].setLayoutParams(new LayoutParams(popupHeight,popupHeight));
			popupbuts[i].setOnClickListener(oclp);
			popupbuts[i].setBackgroundDrawable(bg);
			popupbuts[i].setVisibility(View.GONE);
			if(i<3){
				altview1.addView(popupbuts[i]);
			}else if(i<7){
				altview2.addView(popupbuts[i]);
			}else{
				altview3.addView(popupbuts[i]);
			}
		}
		popupll.addView(altview1);
		popupll.addView(altview2);
		popupll.addView(altview3);
		return popupll;
	}
	//Uzun basma
	private void LongClick(View p1, MotionEvent p2){
		if(((key)p1).popupList.length>1){
			if(prePop!=null){
				h.post(prePop);
			}
			if(((key)p1).popupList.length<3){
				if(((key)p1).popupList[1].contains("::")){
					getCurrentInputConnection().sendKeyEvent(setctrlalt(Integer.parseInt(((key)p1).popupList[1].split("::")[0]),ctrl,alt,false));
				}else{
					getCurrentInputConnection().commitText(((key)p1).popupList[1],1);
				}
			}else{
				popupll.setVisibility(View.VISIBLE);
				for(i=0;i<popupbuts.length-1;i++){
					if(i<((key)p1).popupList.length){
						String keylabel=((key)p1).popupList[i];
						if(keylabel.contains("::")){
							popupbuts[i].setTag(keylabel.split("::")[0]);
							popupbuts[i].setText(keylabel.split("::")[1]);
						}else{
							popupbuts[i].setText(keylabel);
						}
						popupbuts[i].setVisibility(View.VISIBLE);
					}else{
						popupbuts[i].setText("");
						popupbuts[i].setTag("");
						popupbuts[i].setVisibility(View.GONE);
					}
				}
			}
			if(postPop!=null){
				h.post(postPop);
			}
		}else{
			if(((key)p1).longClick!=null){
				h.post(((key)p1).longClick);
			}
		}
	}
	//popup basma eventi
	OnClickListener oclp = new OnClickListener(){
		@Override
		public void onClick(View p1){
			interract(p1);
			vibrate();
			popupll.setVisibility(View.GONE);
		}
	};
	public LinearLayout filler(View v){
		LinearLayout ll =new LinearLayout(ctx);
		ll.setLayoutParams(new LayoutParams(-1,popupHeight*6));
		ll.setGravity(Gravity.CENTER);
		ll.addView(v);
		if(toucharea!=null){
			ll.setOnTouchListener(toucharea);
		}
		return ll;
	}
	//işe yarar şeyler bura
	//, yerine &v kullanılmalı
	String[] decode(String encoded){
		String[] decoded=(encoded).replaceAll("&","&a").split(",");
		for(int i=0;i<decoded.length;i++){
			decoded[i]=decoded[i].replaceAll("&a","&").replaceAll("&v",",").replaceAll("&V",",");
		}
		return decoded;
	}
	public void vibrate(){
		Vibrator vib = (Vibrator) ctx.getSystemService(ctx.VIBRATOR_SERVICE);
		vib.vibrate(vibration);
	}
	public String[][][] UpperCase(String[][][] array){
		for(int i=0;i<array.length;i++){
			for(int j=0;j<array[i].length;j++){
				for(int k=0;k<array[i][j].length;k++){
					array[i][j][k]=array[i][j][k].toUpperCase(Locale.getDefault());
				}
			}
		}
		return array;
	}
	public String[][] UpperCase(String[][] array){
		for(int i=0;i<array.length;i++){
			for(int j=0;j<array[i].length;j++){
				array[i][j]=array[i][j].toUpperCase(Locale.getDefault());
			}
		}
		return array;
	}
	public String[][][] LowerCase(String[][][] array){
		for(int i=0;i<array.length;i++){
			for(int j=0;j<array[i].length;j++){
				for(int k=0;k<array[i][j].length;k++){
					array[i][j][k]=array[i][j][k].toLowerCase(Locale.getDefault());
				}
			}
		}
		return array;
	}
	public String[][] LowerCase(String[][] array){
		for(int i=0;i<array.length;i++){
			for(int j=0;j<array[i].length;j++){
				array[i][j]=array[i][j].toLowerCase(Locale.getDefault());
			}
		}
		return array;
	}
	// " " yerine &b "," yerine &v koyulur. 
	//Çift boşluk " " üretir.
	//her satır birer row olur.
	String[][] readData(String data){
		String[] lines=data.split("\n");
		String[][] readed=new String[lines.length][];
		for(int i=0;i<lines.length;i++){
			readed[i]=lines[i].split(" ");
			for(int j=0;j<readed[i].length;j++){
				readed[i][j].replaceAll("&v",",").replaceAll("&b"," ").replaceAll("&a","&");
				if(readed[i][j].length()<1){
					readed[i][j]=" ";
				}
			}
		}
		return readed;
	}
}
